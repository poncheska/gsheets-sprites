module github.com/poncheska/gsheets-sprites

go 1.14

require (
	github.com/stretchr/testify v1.7.0
	golang.org/x/oauth2 v0.0.0-20210628180205-a41e5a781914
	google.golang.org/api v0.50.0
)
