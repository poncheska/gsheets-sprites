package main

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestServiceAccount(t *testing.T) {
	tests := []struct{
		credFile string
		ok bool
	}{
		{
			"",
			false,
		},
		{
			"1",
			false,
		},
	}

	for i,v := range tests{
		t.Run(fmt.Sprintf("Test%v",i), func(t *testing.T) {
			_, err := ServiceAccount(v.credFile)
			if v.ok {
				assert.Nil(t, err)
			} else {
				assert.NotNil(t, err)
			}
		})
	}
}